package com.otus.patterns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuelCommandApplication {

  public static void main(String[] args) {
    SpringApplication.run(FuelCommandApplication.class, args);
  }

}
